
const routes = [
  {
    path: '/',
    redirect: {
      name: 'user-list',
    },
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: 'user-list',
        name: 'user-list',
        component: () => import('pages/UserList.vue'),
      },
      {
        path: 'user-add',
        name: 'user-add',
        component: () => import('pages/AddUserPage.vue'),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
